# cMENU

可以简单的创建一个终端菜单的工具

## 目录
- [cMENU](#cmenu)
  - [目录](#%e7%9b%ae%e5%bd%95)
  - [使用方法](#%e4%bd%bf%e7%94%a8%e6%96%b9%e6%b3%95)
    - [欢迎使用 cMENU](#%e6%ac%a2%e8%bf%8e%e4%bd%bf%e7%94%a8-cmenu)
    - [构建](#%e6%9e%84%e5%bb%ba)
      - [将源码文件添加至您的项目中](#%e5%b0%86%e6%ba%90%e7%a0%81%e6%96%87%e4%bb%b6%e6%b7%bb%e5%8a%a0%e8%87%b3%e6%82%a8%e7%9a%84%e9%a1%b9%e7%9b%ae%e4%b8%ad)
    - [数据结构](#%e6%95%b0%e6%8d%ae%e7%bb%93%e6%9e%84)
      - [cOPTION](#coption)
      - [cMENU](#cmenu-1)
    - [使用 cOPTION 和 cMENU](#%e4%bd%bf%e7%94%a8-coption-%e5%92%8c-cmenu)
      - [cMENU 中的构建方式](#cmenu-%e4%b8%ad%e7%9a%84%e6%9e%84%e5%bb%ba%e6%96%b9%e5%bc%8f)
      - [如果你希望修改cMENU中的一些已有的字符串常量](#%e5%a6%82%e6%9e%9c%e4%bd%a0%e5%b8%8c%e6%9c%9b%e4%bf%ae%e6%94%b9cmenu%e4%b8%ad%e7%9a%84%e4%b8%80%e4%ba%9b%e5%b7%b2%e6%9c%89%e7%9a%84%e5%ad%97%e7%ac%a6%e4%b8%b2%e5%b8%b8%e9%87%8f)
      - [选项功能函数的规范](#%e9%80%89%e9%a1%b9%e5%8a%9f%e8%83%bd%e5%87%bd%e6%95%b0%e7%9a%84%e8%a7%84%e8%8c%83)
      - [开始cMENU菜单](#%e5%bc%80%e5%a7%8bcmenu%e8%8f%9c%e5%8d%95)
  - [开源声明](#%e5%bc%80%e6%ba%90%e5%a3%b0%e6%98%8e)
  - [](#)
## 使用方法

### 欢迎使用 cMENU

### 构建
您可以使用以下的方法来将cMENU添加至您的项目中

#### 将源码文件添加至您的项目中
整个cMENU工具仅含有两个文件,您可以直接将 `menu.h` 和 `menu.c` 添加到您的项目中并开始使用.
可以使用如下方式来将cMENU添加至您的源代码中
``` c
#include "menu.h"
```

为了尽可能多的支持各种平台和编译器，cMENU是使用ANSI C (C89)标准编写。

### 数据结构
`cOPTION` 代表了一个选项对象
`cMENU` 代表了一个菜单对象
#### cOPTION
```c
/* The cOPTION structrue: */
typedef struct _one_option cOPTION;
typedef struct _one_option
{
    char *text;
    cMENU *parent_menu;
    cMENU *next_menu;
    int (*function)(void);
}cOPTION;
```
您可以使用 `mk_option(char*,function)` 函数来创建一个option对象(**function参数为一个函数的函数名，且该函数为int类型，且无参数**)
您可以使用以下方式来创建一个选项列表（选项列表是创建菜单的必要参数）:
```c
cOPTION option_list[N]={
    mk_option("option1",func1),
    mk_option("option2",func2),
    ...
    mk_option("optionN",funcN),
}
```
#### cMENU
```c
/* The cMENU structrue */
typedef struct _menu cMENU;
typedef struct _menu
{
    char *name;
    char *text;
    struct _menu *parent_menu;
    cOPTION *options_list;
    int option_number;
    boolean back_available;
}cMENU;
```
您可以使用 `mk_menu(char*,char*,cMENU*,cOPTION*,int,boolean)`函数来创建一个option对象(**通常来说，推荐您在第三个参数处始终使用 NULL**)
可以使用下述方式来创建一个菜单对象
```c
cMENU menu = mk_menu("menu_title","menu_introduction",NULL,option_list,N,TRUE);
```

### 使用 cOPTION 和 cMENU

#### cMENU 中的构建方式
* **cMENU** 由 `mk_menu` 构建
* **cOPTION**  由 `mk_option` 构建
* **cOPTION[]** 推荐使用以下方式构建:
``` c
cOPTION option_list[N] = {
    mk_option,
    mk_option,...
}
```
#### 如果你希望修改cMENU中的一些已有的字符串常量
* `modify_system_speaker`  `modify_system_speaker(char*)` 可以修改菜单系统对自己的说明默认为：`<|系统提示|>`
* `modify_menu_reminder`  `modify_menu_reminder(char*)` 可以修改菜单中菜单说明的提示符，默认为： `菜单说明`
* `modify_whether_remind`  `modify_whether_remind(char*)` 可以修改在错误输入后对是否重新输入的提示符，默认为`是否重新输入`
* `modify_input_remind`  `modify_input_remind(char*)` 可以修改系统对请求选项的提示符，默认为 `请输入选项`
* `modify_back_choice`  `modify_back_choice(char*)` 可以修改系统对返回选项的描述，默认为 `返回上层菜单`
* `link_menu2option` 如果您希望在选项执行结束后可以去到下一个菜单，请使用 `link_menu2option` 函数，将某个选项与某个菜单链接。这样在该选项执行结束后会跳转至该菜单。

#### 选项功能函数的规范
你可以创建函数给选项使用，在用户选择该选项后，会自动执行该函数.如果选项不需要执行某些功能，请在构造时将其功能函数参数处使用`NONE_OPTION_FUNC`参数:`mk_option("**",NONE_OPTION_FUNC)`(**NONE_OPTION_FUNC**).
**功能函数编写规范.**
```c
int func_name(void)
{
    statements;
    return NORMAL;//如果返回值为NORMAL 则会正常进入下一级菜单
    /*return BACK; //如果返回值为BACK 则会退回到原菜单
}
```

#### 开始cMENU菜单
* `start` 如果你已经完成了菜单的构建，并准备启动该菜单，您可以使用 `start(cMENU*)`函数来启动菜单.该函数需要您指定最初的菜单.如果菜单正常结束. `start`函数将会返回**0** .

## 开源声明

MIT License

>  Copyright (c) 2009-2017 GuoZi
>
>  Permission is hereby granted, free of charge, to any person obtaining a copy
>  of this software and associated documentation files (the "Software"), to deal
>  in the Software without restriction, including without limitation the rights
>  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
>  copies of the Software, and to permit persons to whom the Software is
>  furnished to do so, subject to the following conditions:
>
>  The above copyright notice and this permission notice shall be included in
>  all copies or substantial portions of the Software.
>
>  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
>  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
>  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
>  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
>  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
>  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
>  THE SOFTWARE.

## 
- 果子 (original author)